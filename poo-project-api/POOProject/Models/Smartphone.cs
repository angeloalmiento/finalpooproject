﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POOProject.Models
{
    public class Smartphone
    {
        public int ID { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Armazenamento { get; set; }
        public int RAM { get; set; }
        public int? CPF_ID {get; set; }
    }
}
