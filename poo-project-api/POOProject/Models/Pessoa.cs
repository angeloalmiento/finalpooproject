﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POOProject.Models
{
    public class Pessoa
    {
        public int ID { get; set; }
        public string CPF { get; set; }
    }
}
